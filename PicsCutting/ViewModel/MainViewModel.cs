using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Microsoft.Win32;
using PicsCutting.Services;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using Image = SixLabors.ImageSharp.Image;
using ResizeMode = SixLabors.ImageSharp.Processing.ResizeMode;


namespace PicsCutting.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IImageService _imageService;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IImageService imageService)
        {
            //            _currentImage =
            //                "https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
            _imageService = imageService;
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

        string _buttonText = "Open Image";

        public string ButtonText
        {
            get
            {
                return _buttonText;

            }
            set
            {
                RaisePropertyChanged(() => ButtonText);
                Set(ref _buttonText, value);
            }
        }

        private RelayCommand _deleteCommand;

        public RelayCommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? new RelayCommand(async () =>
                {
                    //                    IsEnabled = false;
                    //                    try
                    //                    {
                    //                        var result = await Service.DeleteKeyAsync(ResourceId, ClientId);
                    //                        OperationStatus = result.ReasonPhrase;
                    //                    }
                    //                    finally
                    //                    {
                    //                        IsEnabled = true;
                    //                    }
                }, () => true);
            }
        }

        private RelayCommand _analyzeImageCommand;


        string COMPUTER_VISION_SUBSCRIPTION_KEY = "7bee070916b14c88ba132f7d6354d2aa";
        string COMPUTER_VISION_ENDPOINT = "https://butekcomputervision.cognitiveservices.azure.com/";
        Microsoft.Azure.CognitiveServices.Vision.ComputerVision.ComputerVisionClient _client;
        public RelayCommand AnalyzeImageCommand
        {
            get
            {
                return _analyzeImageCommand ?? new RelayCommand(async () =>
                {
                    var client = _client ?? Authenticate(COMPUTER_VISION_ENDPOINT, COMPUTER_VISION_SUBSCRIPTION_KEY);
                    _client = client;
                    List<VisualFeatureTypes> features = new List<VisualFeatureTypes>()
                    {
                        VisualFeatureTypes.Categories, VisualFeatureTypes.Description,
                        VisualFeatureTypes.Faces, VisualFeatureTypes.ImageType,
                        VisualFeatureTypes.Tags, VisualFeatureTypes.Adult,
                        VisualFeatureTypes.Color, VisualFeatureTypes.Brands,
                        VisualFeatureTypes.Objects
                    };

                    using (var stream = new MemoryStream())
                    {
                        Image.SaveAsJpeg(stream);
                        stream.Position = 0;
                        var results = await client.AnalyzeImageInStreamAsync(stream, features);

                        foreach (var face in results.Faces)
                        {
                            var image = Image as Image<Rgba32>;
                            var points = new[] {
                                new PointF(face.FaceRectangle.Left, face.FaceRectangle.Top),
                                new PointF(face.FaceRectangle.Left + face.FaceRectangle.Width, face.FaceRectangle.Top),
                                new PointF(face.FaceRectangle.Left + face.FaceRectangle.Width, face.FaceRectangle.Top +face.FaceRectangle.Height),
                                new PointF(face.FaceRectangle.Left, face.FaceRectangle.Top+face.FaceRectangle.Height),

                            };
                            image.Mutate(i => i.DrawPolygon(Rgba32.LightGreen, 2, points));
                            var font = SixLabors.Fonts.SystemFonts.CreateFont("Arial", 12);
                            image.Mutate(i => i.DrawText($"age: {face.Age}", font, Rgba32.Red, new PointF(face.FaceRectangle.Left, face.FaceRectangle.Top)));
                        }


                        foreach (var face in results.Objects)
                        {
                            var image = Image as Image<Rgba32>;
                            var points = new[] {
                                new PointF(face.Rectangle.X, face.Rectangle.Y),
                                new PointF(face.Rectangle.X + face.Rectangle.W, face.Rectangle.Y),
                                new PointF(face.Rectangle.X + face.Rectangle.W, face.Rectangle.Y +face.Rectangle.H),
                                new PointF(face.Rectangle.X, face.Rectangle.Y+face.Rectangle.H),

                            };
                            image.Mutate(i => i.DrawPolygon(Rgba32.LightGreen, 2, points));
                            var font = SixLabors.Fonts.SystemFonts.CreateFont("Arial", 12);
                            image.Mutate(i => i.DrawText($"{face.ObjectProperty}", font, Rgba32.Red, new PointF(face.Rectangle.X, face.Rectangle.Y)));
                        }

                    }
                    UpdateImage(Image);
                    RaisePropertyChanged();
                });
            }
        }

        public static ComputerVisionClient Authenticate(string endpoint, string key)
        {
            ComputerVisionClient client =
              new ComputerVisionClient(new ApiKeyServiceClientCredentials(key))
              { Endpoint = endpoint };
            return client;
        }

        private RelayCommand _openCommand;

        public SixLabors.ImageSharp.Image<Rgba32> Image { get; set; }
        public RelayCommand OpenCommand
        {
            get
            {
                return _openCommand ?? new RelayCommand(async () =>
                {
                    var dialog = new OpenFileDialog();
                    var result = dialog.ShowDialog();
                    if (result.HasValue && result.Value)
                    {


                        var image = SixLabors.ImageSharp.Image.Load<Rgba32>(dialog.FileName);
                        Image = image;
                        image.Metadata.HorizontalResolution = 96;
                        image.Metadata.VerticalResolution = 96;
                        UpdateImage(image);
                    }
                }, () => true);
            }
        }

        private void UpdateImage(Image<Rgba32> image)
        {
            var stream = new MemoryStream();
            image.SaveAsJpeg(stream);
            stream.Position = 0;
            if (_currentImage != null)
                _currentImage.StreamSource.Dispose();

            _currentImage = new BitmapImage();
            ((BitmapImage)_currentImage).BeginInit();
            ((BitmapImage)_currentImage).StreamSource = stream;
            ((BitmapImage)_currentImage).EndInit();

            RaisePropertyChanged(() => CurrentImage);
        }

        public RelayCommand _resizeCommand;
        public RelayCommand ResizeCommand
        {
            get
            {
                return _resizeCommand ?? new RelayCommand(async () =>
                {
                    //                    Image.Mutate(c => c.Vignette(Color.Aquamarine));
                    Resize(NewWidth, NewHeight, Image);


                    UpdateImage(Image);
                    RaisePropertyChanged();
                }, () => true);
            }
        }
        private BitmapImage _currentImage;

        private void Resize(int width, int height, Image<Rgba32> image)
        {
            var k = (double)image.Width / image.Height;
            var kn = (double)width / height;


            var firstSidePixel = image[0, 0];
            var secondSidePixel = image[image.Width - 1, image.Height - 1];
            //if new image will be narrower than old one
            if (kn < k)
            {

                var firstOccurrenceX = CheckLeftAndRightSides(image, kn, firstSidePixel, secondSidePixel);
                if (firstOccurrenceX != int.MaxValue && firstOccurrenceX != 0)
                    image.Mutate(c => c.Crop(new Rectangle(firstOccurrenceX, 0, image.Width - 2 * firstOccurrenceX, image.Height)));
            }

            //if new image will be wider than old one
            if (kn >= k)
            {

                var firstOccurrenceY = CheckTopAndBottomSides(image, kn, firstSidePixel, secondSidePixel);
                if (firstOccurrenceY != int.MaxValue && firstOccurrenceY != 0)
                    image.Mutate(c => c.Crop(new Rectangle(0, firstOccurrenceY, image.Width, image.Height - 2 * firstOccurrenceY)));
            }


            image.Mutate(c => c.Resize(options: new ResizeOptions
            {
                Mode = ResizeMode.Max,
                Size = new SixLabors.Primitives.Size(width, height)
            }));

        }

        private int CheckLeftAndRightSides(Image<Rgba32> image, double kn, Rgba32 firstSidePixel, Rgba32 secondSidePixel)
        {
            var isDifferent = false;
            var cutWidth = (image.Width - kn * image.Height) / 2;
            int firstOccurrenceX = int.MaxValue;
            for (int y = 0; y < image.Height - 1; y++)
            {
                for (int x = 0; x < cutWidth - 1; x++)
                {
                    //check left side
                    if (!ComparePixel(firstSidePixel, image[x, y]) ||
                        !ComparePixel(secondSidePixel, image[image.Width - x - 1, y]))
                    {
                        isDifferent = true;
                        firstOccurrenceX = Math.Min(x, firstOccurrenceX);
                    }
                }
            }
            if (isDifferent)
                return firstOccurrenceX;
            return (int)cutWidth;
        }

        private int CheckTopAndBottomSides(Image<Rgba32> image, double kn, Rgba32 firstSidePixel, Rgba32 secondSidePixel)
        {
            var isDifferent = false;
            var cutWidth = (image.Height - image.Width / kn) / 2;
            int firstOccurrenceY = int.MaxValue;
            for (int x = 0; x < image.Width - 1; x++)
            {
                for (int y = 0; y < cutWidth - 1; y++)
                {
                    //check left side
                    if (!ComparePixel(firstSidePixel, image[x, y]) ||
                        !ComparePixel(secondSidePixel, image[x, image.Height - y - 1]))
                    {
                        isDifferent = true;
                        firstOccurrenceY = Math.Min(y, firstOccurrenceY);
                    }
                }
            }

            if (isDifferent)
                return firstOccurrenceY;
            return (int)cutWidth;

        }

        public double Tolerance { get; set; } = 30;
        private int _newWidth = 330;
        private int _newHeight = 210;

        public int NewWidth
        {
            get => _newWidth;
            set => Set(ref _newWidth, value);
        }

        public int NewHeight
        {
            get => _newHeight;
            set => Set(ref _newHeight, value);
        }

        bool ComparePixel(Rgba32 pixel1, Rgba32 pixel2)

        {
            var tolerance = byte.MaxValue / (double)100 * Tolerance;
            return (Math.Abs(pixel1.A - pixel2.A) < tolerance)
                   && (Math.Abs(pixel1.R - pixel2.R) < tolerance)
                   && (Math.Abs(pixel1.G - pixel2.G) < tolerance)
                   && (Math.Abs(pixel1.B - pixel2.B) < tolerance);
        }
        public BitmapImage CurrentImage => _currentImage;
    }
}